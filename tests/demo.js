module.exports = {
    'passed-test' : function (browser) {
      browser
        .url('http://localhost:3000')
        .waitForElementVisible('body', 2000)
        .setValue('input[name=fullName]', 'nightwatch')
        .setValue('input[name=email]', 'nightwatch@ymail.com')
        .setValue('input[name=password]', 'ahmedgouda1234')
        .setValue('input[name=confirmPassword]', 'ahmedgouda1234')
        .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.jpg')
        .waitForElementVisible('button[type=submit]', 2000)
        .pause(1000)
        .click('button[type=submit]')
        .pause(3000)
        .end();
    },
    'invalid-email' : function (browser) {
        browser
          .url('http://localhost:3000')
          .waitForElementVisible('body', 2000)
          .setValue('input[name=fullName]', 'nightwatch')
          .setValue('input[name=email]', 'nightwatchymail.com')
          .setValue('input[name=password]', 'ahmedgouda1234')
          .setValue('input[name=confirmPassword]', 'ahmedgouda1234')
          .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.jpg')
          .waitForElementVisible('button[type=submit]', 2000)
          .pause(1000)
          .click('button[type=submit]')
          .pause(3000)
          .end();
      }, 
      'invalid-password' : function (browser) {
        browser
          .url('http://localhost:3000')
          .waitForElementVisible('body', 2000)
          .setValue('input[name=fullName]', 'nightwatch')
          .setValue('input[name=email]', 'nightwatch@ymail.com')
          .setValue('input[name=password]', 'ahmd4')
          .setValue('input[name=confirmPassword]', 'ahmd4')
          .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.jpg')
          .waitForElementVisible('button[type=submit]', 2000)
          .pause(1000)
          .click('button[type=submit]')
          .pause(3000)
          .end();
      },
      'invalid-confirm-password' : function (browser) {
        browser
          .url('http://localhost:3000')
          .waitForElementVisible('body', 2000)
          .setValue('input[name=fullName]', 'nightwatch')
          .setValue('input[name=email]', 'nightwatch@ymail.com')
          .setValue('input[name=password]', 'ahmed1234')
          .setValue('input[name=confirmPassword]', 'ahmedgouda1234')
          .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.jpg')
          .waitForElementVisible('button[type=submit]', 2000)
          .pause(1000)
          .click('button[type=submit]')
          .pause(3000)
          .end();
      },
      'test-upload-media' : function (browser) {
        browser
          .url('http://localhost:3000')
          .waitForElementVisible('body', 2000)
          .setValue('input[name=fullName]', 'nightwatch')
          .setValue('input[name=email]', 'nightwatch@ymail.com')
          .setValue('input[name=password]', 'ahmedgouda1234')
          .setValue('input[name=confirmPassword]', 'ahmedgouda1234')
          .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.jpg')
          .waitForElementVisible('button[type=submit]', 2000)
          .pause(1000)
          .click('button[id=cancelFile]')
          .pause(2000)
          .setValue('input[type=file]', 'C:/xampp/htdocs/WYF/uploads/1640.png')
          .pause(2000)
          .click('button[type=submit]')
          .pause(3000)
          .end();
      },
  };