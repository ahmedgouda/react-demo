import React , { Component }from 'react';
import { Register } from "./register";
import { Profile } from "./profile"
import { BrowserRouter as Router } from "react-router-dom"
import Route from "react-router-dom/Route";
import './App.css';


class App extends Component {
  
  render() {
    return (
      <div>
      <Router>
     <Route path="/"  exact strict component = {Register}  />
     
</Router>
<Router>
     <Route path="/profile" exact strict component = {Profile}  />
     
</Router>
</div>
    );
  }
}

export default App;
