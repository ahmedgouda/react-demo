import React , { Component }from 'react';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { Form } from "./form";
import './App.css';


export class Register extends Component {
  state = {
    fields: {}
  };

  onChange = updatedValue => {
    this.setState({
      fields: {
        ...this.state.fields,
        ...updatedValue
      }
    });
  };
  
  render() {
    return (
      <MuiThemeProvider>
            <div className="App">
      <Form action="profile" method="post" onChange={fields => this.onChange(fields)} />
      <p>
        {JSON.stringify(this.state.fields, null, 2)}
      </p>
    </div>
</MuiThemeProvider>
    );
  }
}
