import React from "react";
import { TextField, FlatButton}  from "material-ui"

export class Form extends React.Component{

    state = {
        fullName : "",
        fullNameErr : "",
        email : "",
        emailErr : "",
        password : "",
        confirmPassword : "",
        confirmPasswordErr : "",
        image:"noimage.jpg",
        imageErr : ""
    }
    
    
      change = e => {
        this.props.onChange({ [e.target.name]: e.target.value });
        this.setState({
          [e.target.name]: e.target.value
        });
      };
  
 
    validate = () =>{
        let isErr = false ; 
        const errors = {
            fullNameErr : "",
             emailErr :"",
            passwordErr : "",
            confirmPasswordErr : "",
            imageErr : "",

        };
        if(typeof this.state.fullName === "undefined"){
            isErr = true;
            errors.fullNameErr = "full name is required";
        }
        else if( this.state.fullName.length < 5){
            isErr = true;
            errors.fullNameErr = "full name must be more that 5 characters";
        }
        if(typeof this.state.email === "undefined"){
            isErr = true;
            errors.emailErr = "email is required";
        }
        else if( this.state.email.indexOf('@') === -1 || this.state.email.indexOf('.com') === -1){
            isErr = true;
            errors.emailErr = "please enter a valid email";
        }
        if(typeof this.state.password === "undefined"){
            isErr = true;
            errors.passwordErr = "password is required";
        }
        else if( this.state.password.length < 6 ){
            isErr = true;
            errors.passwordErr = "password must be more than 6 characters";
        }
        if(typeof this.state.confirmPassword === "undefined"){
            isErr = true;
            errors.confirmPasswordErr = "confirm password is required";
        }
        else if(this.state.confirmPassword !== this.state.password){
            isErr = true;
            errors.confirmPasswordErr = "password doesn't match";
        }
        if(typeof this.state.image === "undefined"){
            isErr = true;
            errors.imageErr = "please upload media";
        }
        
            this.setState({
                ...this.state,
                ...errors
            });
        
        return isErr;

    }
    
  onSubmit = e => {
    e.preventDefault();
    const err = this.validate();
    if(!err){
        //store data then redirect
       window.location.href = this.props.action;
    }

  };

  fileSelected = e =>{
      this.setState({
          image : URL.createObjectURL(e.target.files[0])
      });
      console.log(e.target.files[0])
  }
  cancel = e=>{
    this.setState({
        image : "noimage.jpg"
    });
  }
    render(){
        return(
            <div className="container">
            <center><h1>Form </h1></center>
        <div className="container">
            <form action={this.props.action} method = {this.props.method}>
                <TextField
                name= "fullName"
                floatingLabelText= "First Name"
                floatingLabelFixed = {true}
                value={this.state.fullName}
                errorText = {this.state.fullNameErr}
                onChange={e => this.change(e)}
                />
        
            <hr/>
            <TextField
                name= "email"
                floatingLabelText= "Email"
                floatingLabelFixed = {true}
                value={this.state.email}
                errorText={this.state.emailErr}
                onChange={e => this.change(e)}
                />
            <hr/>
           
            <TextField
                name= "password"
                type="password"
                floatingLabelText= "Password"
                floatingLabelFixed = {true}
                value={this.state.password}
                errorText={this.state.passwordErr}
                onChange={e => this.change(e)}
                />
            <hr/>
            <TextField
                name= "confirmPassword"
                type="password"
                floatingLabelText= "Confirm Password"
                floatingLabelFixed = {true}
                value={this.state.confirmPassword}
                errorText={this.state.confirmPasswordErr}
                onChange={e => this.change(e)}
                />
                <hr/>
                <input type= "file" style={ {display: 'none'} } 
                onChange = {this.fileSelected}
                ref={fileInput => this.fileInput = fileInput }
                />
                <img src={this.state.image} style={{width : '30%',height : '30%'}}/>
                <hr/>
                <FlatButton style={{color : 'blue'}} id="uploadFile"  onClick = {() => this.fileInput.click()}>Upload </FlatButton>   
                <FlatButton style={{color : 'red'}} id="cancelFile"  onClick = {() => this.cancel()}>Cancel </FlatButton>   
            <hr/>
            <button type="submit" className="btn btn-primary" onClick={e => this.onSubmit(e)}> Submit </button>
            </form>
        </div>
        </div>
            );
    }

}
